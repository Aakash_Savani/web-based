const GameState = Object.freeze({
    WELCOMING: Symbol("welcoming"),
    FLAT: Symbol("flat"),
    WAIT: Symbol("wait"),
    MANSION: Symbol("mansion"),
    BUTLER: Symbol("butler"),
    TOAST: Symbol("toast"),
    CONTINUE: Symbol("y"),
    EXIT: Symbol("n"),
    PUZZLE:Symbol("puzzle"),
    WORD:Symbol("word")
});
let sum=0;
let username="";
var commonWords = [
    "the", "of", "and", "a", "to", "in", "is", "you", "that", "it", "he",
    "was", "for", "on", "are", "as", "with", "his", "they", "I", "at", "be",
    "this", "have", "from", "or", "one", "had", "by", "word", "but", "not",
    "what", "all", "were", "we", "when", "your", "can", "said", "there",
    "use", "an", "each", "which", "she", "do", "how", "their", "if", "will",
    "up", "other", "about", "out", "many", "then", "them", "these", "so",
    "some", "her", "would", "make", "like", "him", "into", "time", "has",
    "look", "two", "more", "write", "go", "see", "number", "no", "way",
    "could", "people", "my", "than", "first", "water", "been", "call",
    "who", "oil", "its", "now", "find", "long", "down", "day", "did", "get",
    "come", "made", "may", "part"
];


// Grabbing Random Word
var getRandomWord = function (array) {
    return array[Math.floor(Math.random() * array.length)];
}

var randomWord = getRandomWord(commonWords);

// Function that submits the values
function Guessword(guessWord) {
    var counter = 10;
    var triedCharacters = [];
    var correctCharacters = [];

    var shorterWordlength = randomWord.length > guessWord.length ? guessWord.length : randomWord.length;

    console.log('guessWord', guessWord);

    for (i = 0; i < shorterWordlength; i++) {
        if (guessWord[i] === randomWord[i]) {
            correctCharacters.push(guessWord[i])
            console.log("correct " + correctCharacters)
        } else {
            triedCharacters.push(guessWord[i])
            console.log("incorrect " + triedCharacters)
        }
    }
    randomWord = getRandomWord(commonWords);
    return randomWord;
}

function calculateSum(){
    let input1=Math.floor(Math.random() * 9) + 1 ;
    let input2=Math.floor(Math.random() * 9) + 1 ;
    sum=input1+input2;
    return input1+"+"+input2;
}

// storing jokes in the array jokes
var jokes = [
    { name: 'Dozen', answer: 'anybody want to let me in?' },
    { name: 'Avenue', answer: 'knocked on this door before?' },
    { name: 'Ice Cream', answer: 'if you don\'t let me in!' },
    { name: 'Adore', answer: 'is between us. Open up!' },
    { name: 'Lettuce', answer: 'in. Its cold out here!' },
    { name: 'Bed', answer: 'you can not guess who I am.' },
    { name: 'Al', answer: 'give you a kiss if you open the door.' },
    { name: 'Olive', answer: 'you!' },
    { name: 'Abby', answer: 'birthday to you!' },
    { name: 'Rufus', answer: 'the most important part of your house.' },
    { name: 'Cheese', answer: 'a cute girl.' },
    { name: 'Wanda', answer: 'hang out with me right now?' },
    { name: 'Ho-ho.', answer: 'You know, your Santa impression could use a little work.' },
    { name: 'Mary and Abbey.', answer: 'Mary Christmas and Abbey New Year!' },
    { name: 'Carmen', answer: 'let me in already!' },
    { name: 'Ya', answer: 'I’m excited to see you too!' },
    { name: 'Scold', answer: 'outside—let me in!' },
    { name: 'Robin', answer: 'you! Hand over your cash!' },
    { name: 'Irish', answer: 'you a Merry Christmas!' },
    { name: 'Otto', answer: 'know whats taking you so long!' },
    { name: 'Needle', answer: 'little help gettin in the door.' },
    { name: 'Luke', answer: 'through the keyhole to see!' },
    { name: 'Justin', answer: 'the neighborhood and thought Id come over.' },
    { name: 'Europe', answer: 'No, you are a poo' },
    { name: 'To', answer: 'To Whom.' },
    { name: 'Etch', answer: 'Bless You!' },
    { name: 'Mikey', answer: 'doesnt fit through this keyhole' }
]

//choosing a random joke from the array
var knock = function () {
    var joke = jokes[Math.floor(Math.random() * jokes.length)]
    return formatJoke(joke)
}

//Formatting the output to return in a new line and plug in the output variables
function formatJoke(joke) {
    return [
        'Knock, knock.',
        'Who’s there?',
        joke.name + '.',
        joke.name + ' who?',
        joke.name + ' ' + joke.answer
    ].join('\n')
}

module.exports = class Game {
    constructor() {
        this.stateCur = GameState.WELCOMING;
    }

    makeAMove(sInput) {
        let sReply = "";
        switch(this.stateCur){
case GameState.WELCOMING:
    sReply="Who?";
    this.stateCur=GameState.MANSION;
    break;
    case GameState.MANSION:
 username=sInput;
    sReply="Hi "+username+"\n You want to play?(y/n)";
    this.stateCur=GameState.CONTINUE;
    break;
case GameState.CONTINUE:
    if(sInput.toLowerCase().match("y")){
sReply="Do you want to play maths puzzles?(y/n)";
this.stateCur=GameState.PUZZLE;
    }
    else
    sReply="See you later!!!";
break;
case GameState.PUZZLE:
    if(sInput.toLowerCase().match("y")){
        sReply=calculateSum();
        this.stateCur=GameState.WAIT;
    }else if(sInput.toLowerCase().match("n")){
        sReply="Do you want to play guess word game?(y/n)";
        this.stateCur=GameState.WORD;
    }
    break;
    case GameState.WORD:
        if(sInput.toLowerCase().match("y")){
           sReply="Please guess the next word";
           this.stateCur=GameState.TOAST;
        }else if(sInput.toLowerCase().match("n")){
            sReply="Do you want to listen knock knock joke?(y/n)";
            this.stateCur=GameState.JOKE;
        }        
        break;
        case GameState.TOAST:
            let word=Guessword(sInput);
            if (sInput.toLowerCase().match(word)) {
                sReply = "Briliant!! "+word;
            }
            else{
                sReply = "Sorry!! the correct word is "+word;
            }     
            sReply+="\n Do you want to play again?(y/n)";
            this.stateCur=GameState.CONTINUE;
            break;
        case GameState.WAIT:
            if(sInput==sum){
                sReply="Your answer is right!!";
            }else{
                sReply="Your answer is wrong!! \n Right answer is "+sInput;
            }
            sReply+="\n Do you want to play again?(y/n)";
            this.stateCur=GameState.CONTINUE;
            break;
            case GameState.JOKE:
                if(sInput.toLowerCase().match("y")){
                    sReply=knock();
                 }else if(sInput.toLowerCase().match("n")){
                     sReply="Do you want to listen knock knock joke?(y/n)";
                     this.stateCur=GameState.JOKE;
                 }     
                 sReply+="\n Do you want to play again?(y/n)";
                 this.stateCur=GameState.CONTINUE;
                break;
    default:
        sReply="See you later!!";
        break;
    }
    return ([sReply]);
}
}