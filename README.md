# Writing a README samples

**Version 1.0.0** — [Change log](CHANGELOG.md)

Code and document samples for the writing a README tutorial.

A sample readme for the Conestoga College program to match the video tutorial on writing READMEs.

---

## Contributors

- Akash Savani <Asavani0662conestogac.on.ca>

---

## License & copyright

© Akash J Savani, Conestoga College 

Licensed under the [MIT License](LICENSE).
